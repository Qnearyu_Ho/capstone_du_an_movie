import {
  SET_DANH_SACH_PHIM,
  SET_FILM_DANG_CHIEU,
  SET_FILM_SAP_CHIEU,
} from "../actions/types/QuanLyPhimType";
import { SET_CHI_TIET_PHIM } from "../actions/types/QuanLyRapType";
const stateDefault = {
  arrFilm: [
    {
      maPhim: 10374,
      tenPhim: "Uncharged",
      biDanh: "uncharged",
      trailer: "https://www.youtube.com/watch?v=eHp3MbsCbMg",
      hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/uncharged_gp01.jpg",
      moTa: "ádfjsaldkfsdf",
      maNhom: "GP01",
      ngayKhoiChieu: "2022-10-03T00:00:00",
      danhGia: 8,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
    {
      maPhim: 10374,
      tenPhim: "Uncharged",
      biDanh: "uncharged",
      trailer: "https://www.youtube.com/watch?v=eHp3MbsCbMg",
      hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/uncharged_gp01.jpg",
      moTa: "ádfjsaldkfsdf",
      maNhom: "GP01",
      ngayKhoiChieu: "2022-10-03T00:00:00",
      danhGia: 8,
      hot: true,
      dangChieu: true,
      sapChieu: true,
    },
  ],
  dangChieu: true,
  sapChieu: true,
  arrFilmDefault: [],
  filmDetail: {},
};

export const QuanLyPhimReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case SET_DANH_SACH_PHIM: {
      state.arrFilm = action.arrFilm;
      state.arrFilmDefault = action.arrFilm;

      return { ...state };
    }
    case SET_FILM_DANG_CHIEU: {
      state.dangChieu = !state.dangChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };
    }
    case SET_FILM_SAP_CHIEU: {
      state.sapChieu = !state.sapChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };
    }
    case SET_CHI_TIET_PHIM: {
      state.filmDetail = action.filmDetail;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
