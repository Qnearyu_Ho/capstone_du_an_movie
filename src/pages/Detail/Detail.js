import React, { useEffect } from "react";
import { Radio, Space, Tabs } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { SET_CHI_TIET_PHIM } from "../../redux/actions/types/QuanLyRapType";
import { layThongTinChiTietPhim } from "../../redux/actions/QuanLyRapActions";
import { NavLink } from "react-router-dom";
import moment from "moment";
import "./Detail.css";
const { TabPane } = Tabs;

export default function Detail(props) {
  const filmDetail = useSelector((state) => state.QuanLyPhimReducer.filmDetail);
  console.log({ filmDetail });
  const dispatch = useDispatch();
  useEffect(() => {
    // lấy thông tin param từ url
    let { id } = props.match.params;
    dispatch(layThongTinChiTietPhim(id));
  }, []);
  return (
    <div className="main w-full">
      <div>
        <div className="grid ml-20 grid-cols-12">
          <div style={{ display: "flex" }} className="col-span-4 col-start-4">
            <div className="mt-40 ml-10 grid grid-cols-1">
              <img
                className=""
                src={filmDetail.hinhAnh}
                style={{ width: "100%" }}
              />
            </div>
            <div className="ml-20 mt-40 mr-2  content">
              <p className="text-sm font-bold">
                Ngày chiếu:{" "}
                {moment(filmDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
              </p>
              <p className="text-3xl leading-7">{filmDetail.tenPhim}</p>
              <p>{filmDetail.moTa}</p>
              <p>{filmDetail.diaChi}</p>
            </div>
          </div>
        </div>

        <Tabs defaultActiveKey="1" centered>
          <TabPane className="font-bold" tab="Lịch chiếu" key="1">
            <div className="mt-5 ml-60 container">
              <Tabs tabPosition={"left"}>
                {filmDetail.heThongRapChieu?.map((htr, index) => {
                  return (
                    <TabPane
                      tab={
                        <div>
                          <img
                            src={htr.logo}
                            className="rounded-full"
                            width={50}
                            height={50}
                          />
                          {htr.tenHeThongRap}
                          {htr.diaChi}
                        </div>
                      }
                      key={index}
                    >
                      {htr.cumRapChieu?.map((cumRap, index) => {
                        return (
                          <div className="mt-5" key={index}>
                            <div className="flex flex-row">
                              <img
                                style={{ width: 50, height: 50 }}
                                src={
                                  "https://dictionary.cambridge.org/vi/images/thumb/cinema_noun_001_02737.jpg?version=5.0.252"
                                }
                              />
                              <div className="ml-2">
                                <p className="ml-2">{cumRap.tenCumRap}</p>
                                <p></p>
                              </div>
                            </div>
                            <div className="thong-tin-lich-chieu grid grid-cols-4">
                              <div className="col-span-1">
                                {cumRap.lichChieuPhim?.map(
                                  (lichChieu, index) => {
                                    return (
                                      <NavLink
                                        to="/"
                                        key={index}
                                        className="col-span-1"
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("hh:mm A")}
                                      </NavLink>
                                    );
                                  }
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </TabPane>
                  );
                })}

                {/* <TabPane tab="Tab 2" key="2">
              Content 1
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              Content 1
            </TabPane> */}
              </Tabs>
            </div>
          </TabPane>
          <TabPane className="font-bold" tab="Thông tin" key="2">
            Thông Tin
          </TabPane>
          <TabPane className="font-bold" tab="Đánh giá" key="3">
            Đánh giá
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}
