import React from "react";

export default function Checkout(props) {
  return (
    <div className="container">
      <div className="grid grid-cols-12">
        <div className="col-span-8"></div>
        <div className="col-span-4">
          <h3 className="text-green-400 text-center"></h3>
          <hr />
          <h3 className="text-xl">Lat mat 48h</h3>
          <p>Dia diem</p>
          <p>Ngay chieu</p>
          <hr />
          <div className="flex flex-row my-5">
            <div className="w-4/5">
              <span className="text-red-400">Ghe</span>
            </div>
            <div className="text-right col-span-1">
              <span className="text-green-800 text-lg">0d</span>
            </div>
          </div>
          <hr />
        </div>
      </div>
    </div>
  );
}
